odoo.define('pulsat_sale_invoice10.pulset', function (require) {
"use strict";

	var Model = require('web.DataModel');
	var ListView = require('web.ListView');

	ListView.List.include({
		init: function (group, opts) {
			var self = this;
	        this.group = group;
	        this.view = group.view;
	        this.session = this.view.session;

	        this.options = opts.options;
	        this.columns = opts.columns;
	        this.dataset = opts.dataset;
	        this.records = opts.records;

	        this.record_callbacks = {
	            'remove': function (event, record) {
	                var id = record.get('id');
	                self.dataset.remove_ids([id]);
	                var $row = self.$current.children('[data-id=' + id + ']');
	                var index = $row.data('index');
	                $row.remove();
	            },
	            'reset': function () { return self.on_records_reset(); },
	            'change': function (event, record, attribute, value, old_value) {
	                var $row;
	                if (attribute === 'id') {
	                    if (old_value) {
	                        throw new Error(_.str.sprintf( _t("Setting 'id' attribute on existing record %s"),
	                            JSON.stringify(record.attributes) ));
	                    }
	                    self.dataset.add_ids([value], self.records.indexOf(record));
	                    // Set id on new record
	                    $row = self.$current.children('[data-id=false]');
	                } else {
	                    $row = self.$current.children(
	                        '[data-id=' + record.get('id') + ']');
	                }
	                if ($row.length) {
	                    var $newRow = $(self.render_record(record));
	                    $newRow.find('.o_list_record_selector input').prop('checked', !!$row.find('.o_list_record_selector input').prop('checked'));
	                    $row.replaceWith($newRow);
	                }
	            },
	            'add': function (ev, records, record, index) {
	                var $new_row = $(self.render_record(record));
	                var id = record.get('id');
	                if (id) { self.dataset.add_ids([id], index); }

	                if (index === 0) {
	                    $new_row.prependTo(self.$current);
	                } else {
	                    var previous_record = records.at(index-1),
	                        $previous_sibling = self.$current.children(
	                                '[data-id=' + previous_record.get('id') + ']');
	                    $new_row.insertAfter($previous_sibling);
	                }
	            }
	        };
	        _(this.record_callbacks).each(function (callback, event) {
	            this.records.bind(event, callback);
	        }, this);

	        this.$current = $('<tbody>')
	            .delegate('input[readonly=readonly]', 'click', function (e) {
	                /*
	                    Against all logic and sense, as of right now @readonly
	                    apparently does nothing on checkbox and radio inputs, so
	                    the trick of using @readonly to have, well, readonly
	                    checkboxes (which still let clicks go through) does not
	                    work out of the box. We *still* need to preventDefault()
	                    on the event, otherwise the checkbox's state *will* toggle
	                    on click
	                 */
	                e.preventDefault();
	            })
	            .delegate('td.o_list_record_selector', 'click', function (e) {
	                e.stopPropagation();
	                var selection = self.get_selection();
	                var checked = $(e.currentTarget).find('input').prop('checked');
	                $(self).trigger(
	                        'selected', [selection.ids, selection.records, ! checked]);
	            })
	            .delegate('td.o_list_record_delete', 'click', function (e) {
	                e.stopPropagation();
	                var $row = $(e.target).closest('tr');
	                $(self).trigger('deleted', [[self.row_id($row)]]);
	                // IE Edge go crazy when we use confirm dialog and remove the focused element
	                if(document.hasFocus && !document.hasFocus()) {
	                    $('<input />').appendTo('body').focus().remove();
	                }
	            })
	            .delegate('td button', 'click', function (e) {
	                e.stopPropagation();
	                var $target = $(e.currentTarget),
	                      field = $target.closest('td').data('field'),
	                       $row = $target.closest('tr'),
	                  record_id = self.row_id($row);
	                if(field == "additional_products"){
	                	var order_id = false;
	                	var main_product_id = false;
	                	new Model("sale.order.line").call('get_products_data',[record_id], {}, {async: false})
		                .then(function(records){
							if(records && records[0]){
								order_id = records[0].sale_order_id;
								main_product_id = records[0].main_product_id
								var html_elmt = "<div style='max-height: 300px;overflow: auto;'><table class='table'>";
		                		html_elmt += "<tr>";
		                		if(self.options.deletable){
		                			html_elmt +=	"<th>Select</th>";
		                		}
		                		html_elmt +=	"<th>Product</th>";
		                		html_elmt +=	"<th>Quantity</th>";
				                html_elmt +=	"<th>Sale Price</th>";
				                html_elmt +="</tr>";
								_.each(records, function(record){
									html_elmt += "<tr>";
									if(self.options.deletable){
			                			html_elmt += 	"<td>";
			                				html_elmt += 	"<input type='checkbox' name='additional_item' class='additional-product-item' id="+record.id+"></input>";
			                			html_elmt += 	"</td>";
									}
		                			html_elmt += 	"<td>";
		                			html_elmt += 		record.product_id[1];
		                			html_elmt += 	"</td>";
		                			html_elmt += 	"<td>";
		                			html_elmt += 		record.quantity;
		                			html_elmt += 	"</td>";
		                			html_elmt += 	"<td>";
		                			html_elmt += 		record.sale_price.toFixed(2);
		                			html_elmt += 	"</td>";
		                			html_elmt += "</tr>";
		                		});
		                		html_elmt +="</table></div>";
		                		if(self.options.deletable){
		                			html_elmt +="<div><button class='btn btn-primary btn-sm add_products'>Add</button></div>";
		                		}
		                		$target.closest('td').popover({
			                		placement : 'right',
			                		trigger: 'manual',
			    	    	        html : true,
			    	    	        title: 'Additional Products <a href="#" class="close" data-dismiss="alert">×</a>',
			    	    	        content: function () {
			    	    	            return html_elmt;
			    	    	        },
			                	})
		                		$target.closest('td').popover('show');
							}else{
								alert("Additional products not found...");
							}
						});
	                	$('.popover').delegate('.add_products','click',function(event){
	                		event.stopImmediatePropagation();
	                		var selected_ids = [];
	                		$('.additional-product-item').each(function() {
	        	        		if($(this).is(':checked')) {
	        	        			if($(this).attr('id')){
	        	        				selected_ids.push(parseInt($(this).attr('id')));
	        	        			}
	        	        		} 
	        	        	});
	                		if(record_id){
        	        			if(selected_ids && selected_ids[0]){
        	        				if(order_id){
        	        					new Model("sale.order.line").call('create_sale_order_lines',[selected_ids, order_id, main_product_id, record_id], {}, {async: false})
            			                .then(function(new_records){
            			                	if(new_records && new_records[0]){
            			                		new_records.map(function(new_record){
            			                			var target_id = false;
            			                			if(new_record && new_record.parent_line_id[0] == record_id){
            			                				target_id = new_record.parent_line_id[0];
            			                			}
            			                			var target = self.records.get(target_id);
            			                			var to = target_id ? self.records.indexOf(target) + 1 : 0;
//            			                            self.records.add(new_record, { at: to, silent: false });
            			                			self.records.add(new_record, {silent: false});
            			                		});
            			                		self.dataset.trigger('dataset_changed');
            			                		$(".popover").popover('hide');
            			                	}
            			                });
        	        				}else{
        	        					alert("Order id not found...");
        	        				}
        	        			}else{
        	        				alert("Warehouse location not found.");
        	        			}
        	        		}
	                	});
	                	$('.popover').delegate('a.close','click',function(event){
	                		$(".popover").popover('hide');
	                	});
	                }else{
	                	if ($target.attr('disabled')) {
		                    return;
		                }
		                $target.attr('disabled', 'disabled');

		                // note: $.data converts data to number if it's composed only
		                // of digits, nice when storing actual numbers, not nice when
		                // storing strings composed only of digits. Force the action
		                // name to be a string
		                $(self).trigger('action', [field.toString(), record_id, function (id) {
		                    $target.removeAttr('disabled');
		                    return self.reload_record(self.records.get(id));
		                }]);
	                }
	            })
	            .delegate('a', 'click', function (e) {
	                e.stopPropagation();
	            })
	            .delegate('tr', 'click', function (e) {
	            	if($('.popover') && !$('.popover')[0]){
	            		var row_id = self.row_id(e.currentTarget);
		                if (row_id) {
		                    e.stopPropagation();
		                    if (!self.dataset.select_id(row_id)) {
		                        throw new Error(_t("Could not find id in dataset"));
		                    }
		                    self.row_clicked(e);
		                }
	            	}
	            });
		},
		get_active_id_and_model: function(){
			var active_model;
        	var active_id;
        	try{
	    		var url = window.location.href;
                if(url.match('model') && url.match('model')[0]){
                	var model = /model=(\D+)/.exec(url)[1].split('&');
        			if(model && model[1]){
        				active_model = model[0];
        			}else {
        				active_model = model[0];
        			}
            	}
                if(url.match('id') && url.match('id')[0]){
            		var active_id = parseInt(/id=(\d+)/.exec(url)[1]);
            	}
                if(active_model && active_id){
                	return {'model':active_model, 'active_id':active_id};
                }else{
                	return false;
                }
	    	}catch(e){
	    		console.log('Err:', e);
	    	}
		},
	});

});