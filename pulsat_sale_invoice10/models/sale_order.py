# -*- coding: utf-8 -*-
#################################################################################
# Author      : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################

from odoo import models, fields, api, _


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def prepare_order_lines(self):
        order_lines = []
        for each in self.order_line.filtered(lambda l: not l.additional_item):
            order_lines.append({'id': each, 'product_id': each.product_id})
            for subline in self.order_line.filtered(lambda l: l.parent_line_id.id == each.id):
                order_lines.append({'id': subline, 'product_id': subline.product_id})
        return order_lines


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    @api.model
    def get_products_data(self, record_id):
        if record_id:
            sale_order_line = self.browse(record_id)
            records = []
            if sale_order_line and sale_order_line.product_id:
                additional_products = sale_order_line.product_id.additional_items_ids
                if additional_products and len(additional_products) > 0:
                    for additional_product in additional_products:
                        records.append({
                            'id': additional_product.id,
                            'product_id': [additional_product.product_id.id, additional_product.product_id.name],
                            'quantity': additional_product.quantity,
                            'sale_price': additional_product.sale_price,
                            'sale_order_id': sale_order_line.order_id.id,
                            'main_product_id': sale_order_line.product_id.id
                        })
                    if records:
                        return records
        return False

    @api.model
    def create_sale_order_lines(self, selected_ids, active_id, main_product_id, parent_line_id):
        if selected_ids and len(selected_ids) > 0:
            product_additional_items = self.env['product.additional.items']
            sale_line_obj = self.env['sale.order.line']
            records = []
            for id in selected_ids:
                rec_id = product_additional_items.browse(id)
                result = sale_line_obj.create({'product_id': rec_id.product_id.id,
                                               'product_uom_qty': rec_id.quantity,
                                               'price_unit': rec_id.sale_price,
                                               'order_id': active_id,
                                               'additional_item': True,
                                               'main_product_id': main_product_id,
                                               'parent_line_id': parent_line_id})
                if result and result.read() and result.read()[0]:
                    records.append(result.read()[0])
            return records
        return False

    @api.multi
    def unlink(self):
        for each in self:
            self.search([('parent_line_id', '=', each.id)]).unlink()
        return super(SaleOrderLine, self).unlink()

    @api.multi
    def _prepare_invoice_line(self, qty):
        res = super(SaleOrderLine, self)._prepare_invoice_line(qty)
        res.update({'additional_item': self.additional_item or False,
                    'main_product_id': self.main_product_id.id if self.main_product_id else False,
                    'sale_order_line_id': self.id})
        return res

    additional_item = fields.Boolean(string="Additional Item")
    main_product_id = fields.Many2one('product.product', string="Main Product")
    parent_line_id = fields.Many2one('sale.order.line', string="Line Ref")

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
