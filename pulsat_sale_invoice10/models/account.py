# -*- coding: utf-8 -*-
#################################################################################
# Author      : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################

from odoo import models, fields, api, _


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def prepare_invoice_lines(self):
        invoice_lines = []
        for each in self.invoice_line_ids.filtered(lambda l: not l.additional_item):
            invoice_lines.append({'id': each, 'product_id': each.product_id})
            for subline in self.invoice_line_ids.filtered(lambda l: l.sale_order_line_id.parent_line_id.id == each.sale_order_line_id.id):
                invoice_lines.append({'id': subline, 'product_id': subline.product_id})
        return invoice_lines


class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    @api.constrains('product_id', 'serial_no')
    def _check_serial_no(self):
        if self.product_id and (
                self.product_id.tracking == 'lot' or self.product_id.tracking == 'serial') and not self.serial_no:
            raise Warning(_('Some products require lots/serial numbers, so you need to specify those first!'))

    @api.multi
    def unlink(self):
        for each in self:
            self.search([('parent_line_id', '=', each.id)]).unlink()
        return super(AccountInvoiceLine, self).unlink()

    additional_item = fields.Boolean(string="Additional Item")
    main_product_id = fields.Many2one('product.product', string="Main Product")
    parent_line_id = fields.Many2one('account.invoice.line', string="Line Ref")
    serial_no = fields.Char(string="Serial No.")
    sale_order_line_id = fields.Many2one('sale.order.line', string="Sale Order Line Ref.")

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
