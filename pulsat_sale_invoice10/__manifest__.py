# -*- coding: utf-8 -*-
#################################################################################
# Author      : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################

{
    'name': 'Pulsat Sale Invoice',
    'summary': 'Sale Invoice',
    'version': '1.0',
    'description': """Sale Invoice.""",
    'author': 'Acespritech Solutions Pvt. Ltd.',
    'category': 'Sales',
    'website': "http://www.acespritech.com",
    'price': 35,
    'currency': 'EUR',
    'depends': ['base', 'account', 'sale', 'stock'],
    'data': [
        'views/pulset.xml',
        'views/product_view.xml',
        'views/account_view.xml',
        'views/sale_order_view.xml',
        'report/print_sale_order_template.xml',
        'report/print_account_invoice_template.xml',
        'report/report.xml',
    ],
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
