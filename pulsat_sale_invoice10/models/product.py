# -*- coding: utf-8 -*-
#################################################################################
# Author      : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################

from odoo import models, fields, api, _


class ProductAdditionalItems(models.Model):
    _name = 'product.additional.items'

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.sale_price = self.product_id.lst_price

    product_id = fields.Many2one('product.product', string="Product")
    main_product_id = fields.Many2one('product.product', string="Main Product")
    quantity = fields.Integer(string="Quantity", default=1)
    sale_price = fields.Float(string="Sale Price")


class ProductVariant(models.Model):
    _inherit = 'product.product'

    additional_items_ids = fields.One2many('product.additional.items', 'main_product_id', string="Additional Items")

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
